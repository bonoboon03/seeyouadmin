import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import member from './modules/member'
import attendance from './modules/attendance'
import money from './modules/money'
import moneyHistory from './modules/money-history'
import board from './modules/board'
import sell from './modules/sell'
import stock from './modules/stock'
import product from './modules/product'
import productOrder from './modules/product-order'
import sellProduct from './modules/sell-product'
import settlement from './modules/settlement'
import income from './modules/income'
import basket from './modules/basket'

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    member,
    attendance,
    money,
    moneyHistory,
    board,
    sell,
    stock,
    product,
    productOrder,
    sellProduct,
    settlement,
    income,
    basket
}
