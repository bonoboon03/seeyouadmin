const BASE_URL = '/api/sell/v1/sell'

export default {
    DO_CREATE: `${BASE_URL}/file-upload`, // set
    DO_LIST: `${BASE_URL}/all/{page}`,
    DO_BILL_NUMBER_DETAIL: `${BASE_URL}/billNumber-detail`,
    DO_REFUND: `${BASE_URL}/refund`, //put

    // DO_LIST: `${BASE_URL}/all`, //get
    // DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    // DO_DETAIL: `${BASE_URL}/{id}`, //get
    // DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    // DO_UPDATE: `${BASE_URL}/{id}`, //put
    // DO_CREATE: `${BASE_URL}/new`, //post
}

