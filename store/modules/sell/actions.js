    import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store, payload) => {
        let paramArray = []
        if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
        if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
        if (payload.params.searchDay != null) paramArray.push(`searchDay=${payload.params.searchDay}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_LIST.replace('{page}', payload.page) + '?' + paramText)
    },
    [Constants.DO_BILL_NUMBER_DETAIL]: (store, payload) => {
        let paramText = `billNumber=${payload.billNumber}`
        return axios.get(apiUrls.DO_BILL_NUMBER_DETAIL + '?' + paramText)
    },
    [Constants.DO_CREATE]: (store, payload) => {
        let data = new FormData()
        data.append('csvFile', payload)
        let config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
        return axios.post(apiUrls.DO_CREATE, data, config)
    },
    [Constants.DO_REFUND]: (store, payload) => {
        let paramText = `billNumber=${payload.billNumber}`

        return axios.put(apiUrls.DO_REFUND + '?' + paramText)
    },
}
