export default {
    DO_CREATE: 'sell/doCreate', // 등록
    DO_LIST: 'sell/doList', // 리스트
    DO_REFUND: 'sell/doRefund',
    DO_DELETE: 'sell/doDelete',
    DO_BILL_NUMBER_DETAIL: 'sell/doBillNumberDetail'
}
