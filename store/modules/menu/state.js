const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-tickets', currentName: '대시보드', link: '/', isShow: true },
            ]
        },
        {
            parentName: '직원관리',
            menuLabel: [
                { id: 'STAFF_ADD', icon: 'el-icon-tickets', currentName: '직원 등록', link: '/member/form', isShow: false },
                { id: 'STAFF_EDIT', icon: 'el-icon-tickets', currentName: '직원 수정', link: '/member/edit', isShow: false },
                { id: 'STAFF_LIST', icon: 'el-icon-tickets', currentName: '직원 리스트', link: '/member/list', isShow: true },
                { id: 'STAFF_DETAIL', icon: 'el-icon-tickets', currentName: '직원 상세정보', link: '/member/detail', isShow: false },
            ]
        },
        {
            parentName: '급여관리',
            menuLabel: [
                { id: 'MONEY_ADD', icon: 'el-icon-tickets', currentName: '급여 등록', link: '/money/form', isShow: false },
                { id: 'MONEY_EDIT', icon: 'el-icon-tickets', currentName: '급여 수정', link: '/money/edit', isShow: false },
                { id: 'MONEY_LIST', icon: 'el-icon-tickets', currentName: '급여 리스트', link: '/money/list', isShow: true },
                { id: 'MONEY_DETAIL', icon: 'el-icon-tickets', currentName: '급여 상세정보', link: '/money/detail', isShow: false },
            ]
        },
        {
            parentName: '급여지급내역관리',
            menuLabel: [
                { id: 'MONEY_HISTORY_ADD', icon: 'el-icon-tickets', currentName: '급여지급내역 등록', link: '/money-history/form', isShow: false },
                { id: 'MONEY_HISTORY_LIST', icon: 'el-icon-tickets', currentName: '급여지급내역 리스트', link: '/money-history/list', isShow: true },
                { id: 'MONEY_HISTORY_DETAIL', icon: 'el-icon-tickets', currentName: '급여지급내역 상세정보', link: '/money-history/detail', isShow: false },
            ]
        },
        {
            parentName: '근태관리',
            menuLabel: [
                { id: 'ATTENDANCE_LIST', icon: 'el-icon-tickets', currentName: '근태 리스트', link: '/attendance/list', isShow: true },
            ]
        },
        {
            parentName: '상품관리',
            menuLabel: [
                { id: 'PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '상품 등록', link: '/product/form', isShow: false },
                { id: 'PRODUCT_EDIT', icon: 'el-icon-tickets', currentName: '상품 수정', link: '/product/edit', isShow: false },
                { id: 'PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '상품 리스트', link: '/product/list', isShow: true },
                { id: 'PRODUCT_DETAIL', icon: 'el-icon-tickets', currentName: '상품 상세정보', link: '/product/list', isShow: false },
            ]
        },
        {
            parentName: '판매상품관리',
            menuLabel: [
                { id: 'SELL_PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '판매상품 등록', link: '/sell-product/form', isShow: false },
                { id: 'SELL_PRODUCT_EDIT', icon: 'el-icon-tickets', currentName: '판매상품 수정', link: '/sell-product/edit', isShow: false },
                { id: 'SELL_PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '판매상품 리스트', link: '/sell-product/list', isShow: true },
                { id: 'SELL_PRODUCT_DETAIL', icon: 'el-icon-tickets', currentName: '판매상품 상세정보', link: '/sell-product/detail', isShow: false },
            ]
        },
        {
            parentName: '발주관리',
            menuLabel: [
                { id: 'PRODUCT_ORDER_ADD', icon: 'el-icon-tickets', currentName: '발주 등록', link: '/product-order/form', isShow: false },
                { id: 'PRODUCT_ORDER_LIST', icon: 'el-icon-tickets', currentName: '발주 리스트', link: '/product-order/list', isShow: true },
            ]
        },
        {
            parentName: '판매관리',
            menuLabel: [
                { id: 'SELL_ADD', icon: 'el-icon-tickets', currentName: '판매 등록', link: '/sell/add', isShow: false },
                { id: 'SELL_LIST', icon: 'el-icon-tickets', currentName: '판매 리스트', link: '/sell/list', isShow: true },
            ]
        },
        {
            parentName: '재고관리',
            menuLabel: [
                { id: 'STOCK_ADD', icon: 'el-icon-tickets', currentName: '재고 등록', link: '/stock/add', isShow: false },
                { id: 'STOCK_LIST', icon: 'el-icon-tickets', currentName: '재고 리스트', link: '/stock/list', isShow: true },
                { id: 'STOCK_EDIT', icon: 'el-icon-tickets', currentName: '재고 수정', link: '/stock/edit'}
            ]
        },
        {
            parentName: '게시판관리',
            menuLabel: [
                { id: 'BOARD_ADD', icon: 'el-icon-tickets', currentName: '게시글 등록', link: '/board/add', isShow: false },
                { id: 'BOARD_LIST', icon: 'el-icon-tickets', currentName: '게시글 리스트', link: '/board/list', isShow: true },
                { id: 'BOARD_EDIT', icon: 'el-icon-tickets', currentName: '게시글 수정', link: '/board/edit', isShow: false },
                { id: 'BOARD_DETAIL', icon: 'el-icon-tickets', currentName: '게시글 상세정보', link: '/board/detail', isShow: false },
            ]
        },
        {
            parentName: '정산관리',
            menuLabel: [
                { id: 'SETTLEMENT_LIST', icon: 'el-icon-tickets', currentName: '정산 리스트', link: '/settlement/list', isShow: true },
            ]
        },
        {
            parentName: '지출관리',
            menuLabel: [
                { id: 'INCOME_ADD', icon: 'el-icon-tickets', currentName: '지출 등록', link: '/income/form', isShow: false },
                { id: 'INCOME_EDIT', icon: 'el-icon-tickets', currentName: '지출 수정', link: '/income/edit', isShow: false },
                { id: 'INCOME_LIST', icon: 'el-icon-tickets', currentName: '지출 리스트', link: '/income/list', isShow: true },
                { id: 'INCOME_DETAIL', icon: 'el-icon-tickets', currentName: '지출 상세정보', link: '/income/detail', isShow: false },
            ]
        },
        {
            parentName: '장바구니관리',
            menuLabel: [
                { id: 'BASKET_ADD', icon: 'el-icon-tickets', currentName: '지출 등록', link: '/basket/form', isShow: false },
                { id: 'BASKET_LIST', icon: 'el-icon-tickets', currentName: '장바구니 리스트', link: '/basket/list', isShow: true },
            ]
        },
        {
            parentName: '마이 메뉴',
            menuLabel: [
                { id: 'MEMBER_PASSWORD', icon: 'el-icon-lock', currentName: '비밀번호 변경', link: '/my-menu/password', isShow: true },
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-lock', currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
