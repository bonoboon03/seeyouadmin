import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store, payload) => {
        let paramArray = []
        if (payload.params.searchName != null) paramArray.push(`searchName=${payload.params.searchName}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_LIST.replace('{page}', payload.page) + '?' + paramText)
    },
    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE.replace('{memberId}', payload.memberId), payload.list)
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        let paramText = `id=${payload.id}`
        return axios.get(apiUrls.DO_DETAIL + '?' + paramText)
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_UPDATE.replace('{id}', payload.id), payload.list)
    },
}
