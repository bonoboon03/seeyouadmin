const BASE_URL = '/api/money/v1/money'

export default {
    DO_LIST: `${BASE_URL}/all/{page}`, //get
    DO_CREATE: `${BASE_URL}/member-id/{memberId}`, //post
    DO_DETAIL: `${BASE_URL}/alls`,
    DO_UPDATE: `${BASE_URL}/{id}`
}
