export default {
    DO_LIST: 'money/doList',
    DO_CREATE: 'money/doCreate',
    DO_DETAIL: 'money/doDetail',
    DO_UPDATE: 'money/doUpdate'
}
