const BASE_URL = '/api/sell/v1/sell-product'

export default {
    DO_LIST: `${BASE_URL}/all/{page}`, //get
    DO_CREATE: `${BASE_URL}/new`,
    DO_DETAIL: `${BASE_URL}/product`,
    DO_UPDATE: `${BASE_URL}/update/{id}`
}
