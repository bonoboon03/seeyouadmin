export default {
    DO_LIST: 'sell-product/doList',
    DO_CREATE: 'sell-product/doCreate',
    DO_DETAIL: 'sell-product/doDetail',
    DO_UPDATE: 'sell-product/doUpdate'
}
