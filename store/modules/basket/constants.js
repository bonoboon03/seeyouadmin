export default {
    DO_LIST: 'basket/doList',
    DO_CREATE: 'basket/doCreate',
    DO_UPDATE: 'basket/doUpdate',
    DO_DELETE: 'basket/doDelete'
}
