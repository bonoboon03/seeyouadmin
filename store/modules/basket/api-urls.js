const BASE_URL = '/api/product/v1/basket'

export default {
    DO_LIST: `${BASE_URL}/product-id/list`, //get
    DO_CREATE: `${BASE_URL}/product-id/{productId}`,
    DO_UPDATE: `${BASE_URL}/{id}`,
    DO_DELETE: `${BASE_URL}/{id}`
}
