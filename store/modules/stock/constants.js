export default {
    DO_DATA: 'stock/doData', // 재고 등록
    DO_LACK_LIST: 'stock/doLackList', // 재고 부족 리스트
    DO_LIST: 'stock/doList', // 재고 리스트
    DO_LIST_DETAIL: 'stock/doListDetail', // 리스트 상세보기
    DO_UPDATE: 'stock/doUpdate', // 재고 수정
}
