const BASE_URL = '/api/product/v1/stock'

export default {
    DO_DATA: `${BASE_URL}/data`, // 재고등록
    DO_LIST: `${BASE_URL}/all/{page}`, // 재고 리스트
    DO_LACK_LIST: `${BASE_URL}/lack-list`,
    DO_LIST_DETAIL: `${BASE_URL}/stock`, // 재고 리스트 상세
    DO_UPDATE: `${BASE_URL}/update/{id}`, // 재고 수정
}

