export default {
    DO_LIST: 'income/doList',
    DO_CREATE: 'income/doCreate',
    DO_DETAIL: 'income/doDetail',
    DO_UPDATE: 'income/doUpdate'
}
