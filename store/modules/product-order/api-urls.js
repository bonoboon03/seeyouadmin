const BASE_URL = '/api/product/v1/product-order'

export default {
    DO_LIST: `${BASE_URL}/list/{page}`, //get
    DO_CREATE: `${BASE_URL}/data`,
    DO_UPDATE: `${BASE_URL}/complete/{id}`
}
