import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store, payload) => {
        let paramArray = []
        if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
        if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
        if (payload.params.searchDay != null) paramArray.push(`searchDay=${payload.params.searchDay}`)
        if (payload.params.searchComplete != null) paramArray.push(`searchComplete=${payload.params.searchComplete}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_LIST.replace('{page}', payload.page) + '?' + paramText)
    },
    [Constants.DO_CREATE]: (store) => {
        return axios.post(apiUrls.DO_CREATE)
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_UPDATE.replace('{id}', payload.id))
    },
}
