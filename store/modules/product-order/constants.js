export default {
    DO_LIST: 'product-order/doList',
    DO_CREATE: 'product-order/doCreate',
    DO_UPDATE: 'product-order/doUpdate'
}
