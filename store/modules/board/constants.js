export default {
    DO_DATA: 'board/doData', // 등록
    DO_LIST: 'board/doList', // 리스트
    DO_DELETE: 'board/doDelete',
    DO_DETAIL: 'board/doDetail', //get
    DO_UPDATE: 'board/doUpdate',
}
