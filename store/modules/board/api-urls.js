const BASE_URL = '/api/board/v1/board'

export default {
    DO_DATA: `${BASE_URL}/data`, // set
    DO_DETAIL: `${BASE_URL}/board`,
    DO_DELETE: `${BASE_URL}/{id}`, //del
    DO_LIST: `${BASE_URL}/all/{page}`,
    DO_UPDATE: `${BASE_URL}/{id}`
}

