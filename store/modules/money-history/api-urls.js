const BASE_URL = '/api/money-history/v1/money-history'

export default {
    DO_LIST: `${BASE_URL}/all/{page}`, //get
    DO_CREATE: `${BASE_URL}/member-id/{memberId}`, //post
    DO_DETAIL: `${BASE_URL}/history`
}
