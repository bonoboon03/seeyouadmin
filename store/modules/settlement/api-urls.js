const BASE_URL = '/api/settlement/v1/settlement'

export default {
    DO_LIST: `${BASE_URL}/all/{page}`, //get
    DO_DETAIL: `${BASE_URL}/settlement`,
    DO_UPDATE: `${BASE_URL}/update`
}
