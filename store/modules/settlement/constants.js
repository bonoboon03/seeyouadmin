export default {
    DO_LIST: 'settlement/doList',
    DO_DETAIL: 'settlement/doDetail',
    DO_UPDATE: 'settlement/doUpdate'
}
