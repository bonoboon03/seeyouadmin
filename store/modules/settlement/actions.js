import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store, payload) => {
        let paramArray = []
        if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
        if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_LIST.replace('{page}', payload.page) + '?' + paramText)
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        let paramArray = []
        paramArray.push(`month=${payload.month}`)
        paramArray.push(`year=${payload.year}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_DETAIL + '?' + paramText)
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        let paramArray = []
        paramArray.push(`month=${payload.month}`)
        paramArray.push(`year=${payload.year}`)
        let paramText = paramArray.join('&')

        return axios.put(apiUrls.DO_UPDATE + '?' + paramText)
    }
}
